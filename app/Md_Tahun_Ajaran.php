<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Md_Tahun_Ajaran extends Model
{
    protected $table = 'md_tahun_ajaran';
    protected $fillable = ['tahun_ajaran', 'desc', 'created_at','updated_at'];
    protected $primaryKey = 'id_tahun_ajaran';
    public $timestamps = true;
}
