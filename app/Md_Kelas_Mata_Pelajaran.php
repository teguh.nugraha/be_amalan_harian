<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Md_Kelas_Mata_Pelajaran extends Model
{
    protected $table = 'md_kelas_mata_pelajaran';
    protected $fillable = ['id_kelas_mata_pelajaran', 'id_kelas', 'id_mata_pelajaran', 'id_guru', 'created_at', 'updated_at'];
    protected $primaryKey = 'id_kelas_mata_pelajaran';
    public $timestamps = true;

    function guru(){
        return $this->hasOne(Md_Guru::class, 'id_guru', 'id_guru');
    }

    function kelas() {
        return $this->hasOne(Md_Kelas::class, 'id_kelas', 'id_kelas')->with('guru');
    }

    function mataPelajaran() {
        return $this->hasOne(Md_Mata_Pelajaran::class, 'id_mata_pelajaran', 'id_mata_pelajaran');
    }

}
